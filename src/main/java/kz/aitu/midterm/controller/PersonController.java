package kz.aitu.midterm.controller;

import kz.aitu.midterm.model.Person;
import kz.aitu.midterm.service.PersonService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@AllArgsConstructor
public class PersonController {
    private PersonService personService;

    @RequestMapping("/")
    public String getIndex() {
        return "index";
    }

    @GetMapping("/api/v2/users")
    public ResponseEntity<?> getPersonList() {
        return ResponseEntity.ok(personService.getPersonList());
    }

    @PostMapping("/api/v2/users")
    public ResponseEntity<?> savePerson(Person person) {
        return ResponseEntity.ok(personService.savePerson(person));
    }

    @DeleteMapping("/api/v2/users/{id}")
    public void deletePersonById(Long id) {
        personService.deletePersonById(id);
    }

    @PutMapping("/api/v2/users")
    public ResponseEntity<?> editPerson(@RequestBody Person person) {
        return ResponseEntity.ok(personService.savePerson(person));
    }

}
