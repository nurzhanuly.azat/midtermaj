package kz.aitu.midterm.service;

import kz.aitu.midterm.model.Person;
import kz.aitu.midterm.repository.PersonRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class PersonService {
    private final PersonRepository personRepository;

    public PersonService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public List<Person> getPersonList() {
        return (List<Person>) personRepository.findAll();
    }

    @Modifying
    @Transactional
    public Person savePerson(Person person) {
        return personRepository.save(person);
    }

    public void deletePersonById(Long id) {
        personRepository.deleteById(id);
    }

}
